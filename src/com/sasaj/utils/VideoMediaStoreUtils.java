package com.sasaj.utils;

import java.io.File;
import java.util.concurrent.ExecutionException;

import com.sasaj.model.Video;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;

/**
 * VideoMediaStoreUtils contains helper methods to access
 * VideoMediaStore to get metadata of Video.  It works with all Uri
 * schemes.
 */
public class VideoMediaStoreUtils {
	
	private static final String TAG = VideoMediaStoreUtils.class.getSimpleName();
    /**
     * Content Uri scheme for Downloads Provider.
     */
    public static final String DOWNLOADS_PROVIDER_PATH =
        "content://downloads/public_downloads";   
       
    /**
     * Gets the Video from MediaMetadataRetriever
     * by a given path of the video file.
     * 
     * @param context
     * @param filePath of video
     * @return Video having the given filePath
     */
    public static Video getVideo(Context context, String filePath) {
        final MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(filePath);
        final String title = new File(filePath).getName();        
        final long duration = Long.parseLong(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
        final String contentType = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_MIMETYPE);
        return new Video(title, duration, contentType);
    }
    
    /** 
     * Get a Video file path from a Uri. This will get the the path
     * for Storage Access Framework Documents, as well as the _data
     * field for the MediaStore and other file-based ContentProviders.
     * 
     * @param context The context. 
     * @param uri The Uri to query. 
     * 
     * return videoFilePath
     */ 
    public static String getPath(final Context context, final Uri uri) {
    	try {
			return new getPathTask().execute(new TaskParams(context, uri.toString())).get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			return null;
		}
    } 
     
    /** 
     * Get the value of the data column for this Uri. This is useful
     * for MediaStore Uris, and other file-based ContentProviders.
     * 
     * @param context The context. 
     * @param uri The Uri to query. 
     * @param selection (Optional) Filter used in the query. 
     * @param selectionArgs (Optional) Selection arguments used in the query. 
     * @return The value of the _data column, which is typically a file path.
     */ 
    static String getVideoDataColumn(Context context,
                                             Uri uri,
                                             String selection,
                                             String[] selectionArgs) {
        // Projection used to query Android Video Content Provider.
        final String[] projection = {
            MediaStore.Video.Media.DATA
        }; 
     
        //Query and get a cursor to Android Video
        // Content Provider.
        try (Cursor cursor =
                 context.getContentResolver().query(uri,
                                                    projection,
                                                    selection,
                                                    selectionArgs,
                                                    null)) {
                // If video is present, get the file path of the video.
                if (cursor != null 
                    && cursor.moveToFirst()) 
                    return cursor.getString(cursor.getColumnIndexOrThrow
                                            (MediaStore.Video.Media.DATA));
            } 

        // No video present. returns null.
        return null; 
    } 
    
    /** 
     * @param uri The Uri to check. 
     * @return Whether the Uri authority is ExternalStorageProvider. 
     */ 
    static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents"
            .equals(uri.getAuthority());
    } 
     
    /** 
     * @param uri The Uri to check. 
     * @return Whether the Uri authority is DownloadsProvider. 
     */ 
    static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents"
            .equals(uri.getAuthority());
    } 
     
    /** 
     * @param uri The Uri to check. 
     * @return Whether the Uri authority is MediaProvider. 
     */ 
    static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents"
            .equals(uri.getAuthority());
    } 
    
    public static String videoPathOnDevice(Context context, String name) {
		try {
			return new videoPathOnDeviceTask().execute(new TaskParams(context, name)).get();
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			return null;
		}
	}
}

class TaskParams {
     Context context;
     String string;

    TaskParams(Context context, String string) {
        this.context = context;
        this.string = string;
    }
}

class videoPathOnDeviceTask extends AsyncTask<TaskParams, Void, String> {
	
	private static final String TAG = videoPathOnDeviceTask.class.getSimpleName();
	
    protected String doInBackground(TaskParams... params) {
    	Context context = params[0].context;
    	String name = params[0].string;
    	Uri uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        String[] projection = { MediaStore.Video.VideoColumns.DATA };
        Cursor c = context.getContentResolver().query(uri, projection, null, null, null);
        int vidsCount = 0;
        if (c != null) {
            vidsCount = c.getCount();
            while (c.moveToNext()) {
            	String s = c.getString(0);
                if (s.contains(name)) {
                	c.close();
                	Log.d(TAG, "Video is on device, no download needed");
                	return s;
                }
            }
            c.close();
        }
        Log.d(TAG, "Video wasn't found on device");
		return null;
    }
    
}

class getPathTask extends AsyncTask<TaskParams, Void, String> {
	
	private static final String TAG = videoPathOnDeviceTask.class.getSimpleName();
	
    protected String doInBackground(TaskParams... params) {
    	Context context = params[0].context;
    	Uri uri = Uri.parse(params[0].string);
        // Check if the version of current device is greater than API 19 (KitKat).
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider 
            if (VideoMediaStoreUtils.isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];     
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory()  + "/" + split[1];
                } 
            } 
            // DownloadsProvider 
            else if (VideoMediaStoreUtils.isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse(VideoMediaStoreUtils.DOWNLOADS_PROVIDER_PATH), Long.valueOf(id));
                return VideoMediaStoreUtils.getVideoDataColumn(context,contentUri,null,null);
            } 
            // MediaProvider 
            else if (VideoMediaStoreUtils.isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split =docId.split(":");
                final Uri contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                final String selection = "_id = ?";
                final String[] selectionArgs = new String[] { split[1] }; 
                // Get the FilePath from Video MediStore
                // for given Uri, selection, selectionArgs.
                return VideoMediaStoreUtils.getVideoDataColumn(context, contentUri, selection, selectionArgs);
            } 
        } 
        // MediaStore (and general) .
        else if ("content".equalsIgnoreCase(uri.getScheme())) 
            return VideoMediaStoreUtils.getVideoDataColumn(context, uri, null,null);
        // File 
        else if ("file".equalsIgnoreCase(uri.getScheme())) 
            return uri.getPath();
        return null; 
    }
    
}
