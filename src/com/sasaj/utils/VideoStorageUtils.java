package com.sasaj.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.IOUtils;

import retrofit.client.Response;
import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

public class VideoStorageUtils {
 
	static final String TAG = VideoStorageUtils.class.getSimpleName();
	
    @SuppressLint("SimpleDateFormat")
    public static Uri getRecordedVideoUri(Context context) {

        if (isExternalStorageWritable()) {
            final File videoStorageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            if (!videoStorageDir.exists()) {
                if (!videoStorageDir.mkdirs()) {
                    return null;
                }
            }
            final String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            final File videoFile = new File(videoStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4");
            Log.d(TAG, "Video file:" + videoFile);
            notifyMediaScanners(context, videoFile);
            return Uri.fromFile(videoFile);
        } else { 
        	Log.d(TAG, "External SDcard is not mounted");
            return null;
        }
    } 
    
    public static File storeVideoInExternalDirectory(Context context, Response response, String videoName) {

        final File file = getVideoStorageDir(videoName);
        if (file != null) {
            try {
                final InputStream inputStream = response.getBody().in();
                final OutputStream outputStream = new FileOutputStream(file);
                IOUtils.copy(inputStream, outputStream);
                outputStream.close();
                inputStream.close();
                notifyMediaScanners(context, file);
                return file;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    // Notifies the MediaScanners after Downloading the Video, so it is immediately available to the user.
    private static void notifyMediaScanners(Context context, File videoFile) {
        MediaScannerConnection.scanFile(context, new String[] { videoFile.toString() }, null,
             new MediaScannerConnection.OnScanCompletedListener() {
                 public void onScanCompleted(String path, Uri uri) { }
             });
    }

    private static boolean isExternalStorageWritable() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    private static File getVideoStorageDir(String videoName) {
        if (isExternalStorageWritable()) {
            final File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            final File file = new File(path,videoName);
            path.mkdirs();
            Log.d(TAG, "Video file:" + path + videoName);
            return file;
        } else {
        	Log.d(TAG, "External SDcard is not mounted");
            return null;
        }
    }

    private VideoStorageUtils() {
        throw new AssertionError();
    }
    
}
