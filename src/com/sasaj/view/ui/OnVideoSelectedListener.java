package com.sasaj.view.ui;

public interface OnVideoSelectedListener {

	
	public void onVideoSelected(int which, boolean downloaded);
	
	
}
