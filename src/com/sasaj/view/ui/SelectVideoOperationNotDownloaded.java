package com.sasaj.view.ui;

import com.sasaj.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class SelectVideoOperationNotDownloaded extends DialogFragment {
	

    /**
     * Position of Download Video Option in List.
     */
    public static final int DOWNLOAD_VIDEO = 0;
    
	/**
     * Position of Rate Video Option in List.
     */
    public static final int RATE_VIDEO = 1;



	private final String TAG = getClass().getSimpleName();


    /**
     * List of options in the dialog.
     */ 
    String [] listItems = {"Download Video", "Rate Video"};

    /**
     * boolean value set to true if video is downloaded on the device.
     */
	private boolean downloaded=false;    
	
    /**
     * Callback that will send the result to Activity that implements
     * it, when the Option is selected.
     */
    private OnVideoSelectedListener mListener;
    
    
    /**
     * Hook method called when a fragment is first attached to its
     * activity. onCreate(Bundle) will be called after this.
     * 
     * @param activity
     */
    @Override
        public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener =
                (OnVideoSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                                         + " must implement OnVideoSelectedListener");
        }
    }
    
    /**
     * This method will be called after onCreate(Bundle) and before
     * onCreateView(LayoutInflater, ViewGroup, Bundle).  The default
     * implementation simply instantiates and returns a Dialog class.
     */
    @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
  
        AlertDialog.Builder builder = 
            new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.title_selected_video)
               .setItems(listItems,
                         new DialogInterface.OnClickListener() {
                             public void onClick(DialogInterface dialog,
                                                 int which) {
                                 mListener.onVideoSelected(which, downloaded);
                             }
                         });
        return builder.create();
    }


}
