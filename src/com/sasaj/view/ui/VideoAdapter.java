package com.sasaj.view.ui;

import com.sasaj.R;
import com.sasaj.provider.VideoContract.VideoEntry;

import android.content.Context;
import android.database.Cursor;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;


public class VideoAdapter extends SimpleCursorAdapter {


	/**
     * Allows access to application-specific resources and classes.
     */

	static String[] from = new String[] { VideoEntry.COLUMN_TITLE, VideoEntry.COLUMN_RATING };
	static int[] to = new int[] {R.id.videoTitle, R.id.videoRating};
	private static Cursor cursor;

    //Constructor
    public VideoAdapter(Context context) {
   
		super(context, R.layout.video_list_item, cursor, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

	}


	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		
		final LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(R.layout.video_list_item, parent, false);
		return v;
		
	}
    

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		
		int titleCol = cursor.getColumnIndex(VideoEntry.COLUMN_TITLE);
		String title = cursor.getString(titleCol);
		
		int ratingCol = cursor.getColumnIndex(VideoEntry.COLUMN_RATING);
		Double rating = cursor.getDouble(ratingCol);
		
		int totalCol = cursor.getColumnIndex(VideoEntry.COLUMN_TOTAL_RATINGS);
		Integer total = cursor.getInt(totalCol);
		
		String ratingFormatted = String.format( "%.3f", rating );
		
		TextView title_text = (TextView) view.findViewById (R.id.videoTitle);
		title_text.setText(title);
		
		TextView rating_text = (TextView) view.findViewById (R.id.videoRating);
		rating_text.setText("Ratings: " + ratingFormatted + "/5 from "+ total +" user(s)");
	} 


}
