package com.sasaj.view;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sasaj.R;
import com.sasaj.common.GenericActivity;
import com.sasaj.common.Utils;
import com.sasaj.presenter.VideoOps;
import com.sasaj.services.DownloadVideoService;
import com.sasaj.services.UploadVideoService;
import com.sasaj.view.ui.OnVideoSelectedListener;
import com.sasaj.view.ui.RateVideoFragment;
import com.sasaj.view.ui.RateVideoFragment.OnVideoRatedListener;
import com.sasaj.view.ui.SelectVideoOperationDownloaded;
import com.sasaj.view.ui.SelectVideoOperationNotDownloaded;
import com.sasaj.view.ui.VideoAdapter;


public class VideoListActivity extends GenericActivity<VideoOps> 
							   implements OnVideoSelectedListener
							   			  ,OnVideoRatedListener{


    /**
     * The Request Code needed in Implicit Intent to get Video from
     * Gallery.
     */
    private final int REQUEST_GET_VIDEO = 1;

	
	/**
	 * FragmentManager instance used for dialog fragments
	 */
	private FragmentManager fm = getFragmentManager();


    private UploadDownloadResultReceiver mUploadDownloadResultReceiver;
 
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		
		
		setContentView(R.layout.activity_video_list);
		
        mUploadDownloadResultReceiver = new UploadDownloadResultReceiver();

        // Register BroadcastReceiver that receives result from
        // UploadVideoService.
        registerReceiver();
        super.onCreate(savedInstanceState, VideoOps.class);
	}
	

    /**
     * Hook method that gives a final chance to release resources and
     * stop spawned threads. onDestroy() may not always be called-when
     * system kills hosting process
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Unregister BroadcastReceiver.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mUploadDownloadResultReceiver);
    }
    

	
	 /**
     * The Broadcast Receiver that registers itself to receive result
     * from UploadVideoService. 
     */
    public class UploadDownloadResultReceiver 
           extends BroadcastReceiver {
        /**
         * Hook method that's dispatched when the UploadService has
         * uploaded the Video.
         */
        @Override
        public void onReceive(Context context,
                              Intent intent) {
            Log.i(TAG, "Broadcast received");
            getOps().getVideoList();
        }
    }

    /**
     * Register a BroadcastReceiver that receives a result from the
     * UploadVideoService.
     */
    private void registerReceiver() {
        // Create an Intent filter that handles Intents from the
        // UploadVideoService.
        IntentFilter intentFilterUpload =
            new IntentFilter(UploadVideoService.ACTION_UPLOAD_SERVICE_RESPONSE);
        intentFilterUpload.addCategory(Intent.CATEGORY_DEFAULT);
        
        // Create an Intent filter that handles Intents from the
        // UploadVideoService.
        IntentFilter intentFilterDownload =
            new IntentFilter(DownloadVideoService.ACTION_DOWNLOAD_SERVICE_RESPONSE);
        intentFilterDownload.addCategory(Intent.CATEGORY_DEFAULT);

        // Register the BroadcastReceiver.
        LocalBroadcastManager.getInstance(this).registerReceiver(mUploadDownloadResultReceiver,
        														 intentFilterUpload);
        LocalBroadcastManager.getInstance(this).registerReceiver(mUploadDownloadResultReceiver,
        				 										 intentFilterDownload);
    }


 
    public void notDownloadedDialog(){
    	new SelectVideoOperationNotDownloaded().show(fm,"selectVideoOperationNotDownloaded");
    }
    
    public void downloadedDialog(){
    	new SelectVideoOperationDownloaded().show(fm,"selectVideoOperationDownloaded");
    }


    /**
     * Starts download/rating operation, depending on which argument
     * 
     * @param which
     */
	@Override
	public void onVideoSelected(int which, boolean downloaded) {
		
		if (!downloaded){

			switch(which){
			
			case SelectVideoOperationNotDownloaded.DOWNLOAD_VIDEO:
				getOps().downloadVideo();
				break;
				
			case SelectVideoOperationNotDownloaded.RATE_VIDEO:
				new RateVideoFragment().show(getFragmentManager(),"rateVideo");
				break;
			}
		} else {

			switch(which){
			
			case SelectVideoOperationDownloaded.PLAY_VIDEO:
				getOps().playVideo();
				break;
				
			case SelectVideoOperationDownloaded.RATE_VIDEO:
				new RateVideoFragment().show(getFragmentManager(),"rateVideo");
				break;
		    }
	    }
		
	}
	


    /**
     * Starts rating operation
     * 
     * @param which
     */
	@Override
	public void onVideoRated(int rating) {
		getOps().rateVideo(rating);
		Utils.showToast(this, "Video rated with:" + Integer.toString(rating) + " stars");
	}
	
	


	private void selectUploadSource() {
        // Create an intent that will start an Activity to
        // get Video from Gallery.
        final Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT)
        											.setType("video/*")
        											.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        // Create an intent that will start an Activity to
        // record Video.
        final Intent recordVideoIntent = new Intent (MediaStore.ACTION_VIDEO_CAPTURE);
        
        // Create chooser intent to display chooser dialog
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Upload video via");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
        					   new Intent[] { recordVideoIntent });
		
        // start activity to get video from gallery/record video and get resulting uri back
        startActivityForResult(chooserIntent, REQUEST_GET_VIDEO);
	}
    


    /**
     * Called after user selects video from gallery
     */ 
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// Check if the Result is Ok and upload the Video from
        // Gallery to server.
    	
 
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_GET_VIDEO){
            	Log.d(TAG,"Uploading video");
                getOps().uploadVideo(data.getData());
            }
        }
		
	}



    /**
     * Hook method called to initialize the contents of the Activity's
     * standard options menu.
     * 
     * @param menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it
        // is present.
        getMenuInflater().inflate(R.menu.main,
                                  menu);
        return true;
    }

    /**
     * Hook method called whenever an item in your options menu is
     * selected
     * 
     * @param item
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.uploadVideo) {
        	selectUploadSource();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }




}
