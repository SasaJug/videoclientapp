package com.sasaj.view;

import java.lang.ref.WeakReference;

import com.sasaj.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;

public class LoginActivity extends Activity {
	
	private EditText mUserName;
	private EditText mPassword;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		setContentView(R.layout.login_page);
		
		mUserName = (EditText)findViewById(R.id.userName);
		mPassword = (EditText)findViewById(R.id.password);
		
		super.onCreate(savedInstanceState);
	}
	
	public void login (View v){
		Editable userNameEditable = mUserName.getText();
		String userName = userNameEditable.toString();
		
		Editable passwordEditable = mPassword.getText();
		String password = passwordEditable.toString();
		
		Intent intent = new Intent (this, VideoListActivity.class);
		intent.putExtra("userName", userName);
		intent.putExtra("password", password);
		
		startActivity(intent);
	}
	
	public void cancelLogin (View v){
		mUserName.setText("");
		mPassword.setText("");
	}

}
