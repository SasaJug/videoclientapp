package com.sasaj.model;




public class AverageVideoRating {


	private long id;

	private final double rating;

	private final long videoId;

	private final int totalRatings;
	
	public AverageVideoRating(Long videoId){
		super();
		this.rating = 0;
		this.videoId = videoId;
		this.totalRatings = 0;
	}

	public AverageVideoRating(double rating, long videoId, int totalRatings) {
		super();
		this.rating = rating;
		this.videoId = videoId;
		this.totalRatings = totalRatings;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getRating() {
		return rating;
	}

	public long getVideoId() {
		return videoId;
	}

	public int getTotalRatings() {
		return totalRatings;
	}

}
