package com.sasaj.services;

import com.sasaj.R;
import com.sasaj.provider.VideoController;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class UploadVideoService extends IntentService {
	
	private final String TAG = getClass().getSimpleName();
	
    /**
     * Custom Action that will be used to send Broadcast to the
     * VideoListActivity.
     */
    public static final String ACTION_UPLOAD_SERVICE_RESPONSE =
                "com.sasaj.services.UploadVideoService.RESPONSE";
	 
    /**
     * Key, used to store the videoId as an EXTRA in Intent.
     */
	private static final String KEY_UPLOAD_VIDEO_URI = "upload_videoUri";

    /**
     * Default Id , if no Id is present in Extras of the received
     * Intent.
     */
    private static final long DEFAULT_VIDEO_ID = 0;
     
    /**
     * It is used by Notification Manager to send Notifications.
     */
    private static final int NOTIFICATION_ID = 1;
    
    /**
     * VideoController mediates the communication between Server and
     * Android Storage.
     */
    private VideoController mController;
    
    /**
     * Manages the Notification displayed in System UI.
     */
    private NotificationManager mNotifyManager;
    
    /**
     * Builder used to build the Notification.
     */
    private Builder mBuilder;
    
	
	public UploadVideoService(String name) {
		super("UploadVideoService");
	}
	
	
	public UploadVideoService() {
		super("UploadVideoService");
	}
	
    
    /**
     * Factory method that makes the explicit intent another Activity 
     * uses to call this Service. 
     * 
     * @param context
     * @param videoId
     * @return
     */
	public static Intent makeIntent(Context context, Uri videoUri) {
		Log.i("Upload Service","Intent created");

        return new Intent(context, 
        		UploadVideoService.class)
        		.setData(videoUri);
	}


	@Override
	protected void onHandleIntent(Intent intent) {
        //Starts the Notification to show the progress
        // of video upload.
		Log.i(TAG,"Starting UploadVideoService");
        startNotification();
        
        // Create VideoController that will mediates the communication
        // between Server and Android Storage.
        mController =
           VideoController.getOrShowLogin(getApplicationContext()); 
        
        // Check if Video Upload is successful.
        if (mController.uploadVideo(intent.getData()))
            finishNotification("Upload complete");
        else
            finishNotification("Upload failed"); 
        // Upload is completed.
        //Send the Broadcast to Activity that the Video
        sendBroadcast();	
		
	}


	private void sendBroadcast() {
		LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ACTION_UPLOAD_SERVICE_RESPONSE)
        .addCategory(Intent.CATEGORY_DEFAULT));
	}


	private void finishNotification(String status) {
        // When the loop is finished, updates the notification.
        mBuilder.setContentText(status) 
        		.setSmallIcon(R.drawable.ic_launcher)
        		.setTicker("Upload Finished");
        
        // Removes the progress bar.
        mBuilder.setProgress (0, 0, false); 
        mNotifyManager.notify(NOTIFICATION_ID,
                              mBuilder.build());
	}


	private void startNotification() {
		 //Gets the access to System Notification Services.
        mNotifyManager = (NotificationManager)
            getSystemService(Context.NOTIFICATION_SERVICE); 

        // Build the Notification and sets an activity indicator
        // for an operation of indeterminate length.
        mBuilder = new NotificationCompat
                       .Builder(this)
        			   .setTicker("Upload Started")
                       .setContentTitle("Video Upload") 
                       .setContentText("Upload in progress") 
                       .setSmallIcon(R.drawable.ic_launcher)
                       .setProgress(0, 0, true);
 
        // Issues the notification
        mNotifyManager.notify(NOTIFICATION_ID,
                              mBuilder.build());
		
	}




}
