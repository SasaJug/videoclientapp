package com.sasaj.provider;

import java.io.File;

import com.sasaj.provider.VideoContract.VideoEntry;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class VideoDatabaseHelper extends SQLiteOpenHelper {
    /**
     * If the database schema is changed, the database version must be
     * incremented.
     */
    private static final int DATABASE_VERSION = 5;

    /**
     * Database name.
     */
    public static final String DATABASE_NAME =
        "video.db";

	public VideoDatabaseHelper(Context context) {
		super(context, context.getCacheDir()
	              + File.separator 
	              + DATABASE_NAME,
	              null, 
	              DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		 // Define an SQL string that creates a table to hold Videos.
        final String SQL_CREATE_VIDEO_TABLE = "CREATE TABLE "
            + VideoEntry.TABLE_NAME + " (" 
            + VideoEntry._ID + " INTEGER PRIMARY KEY, " 
            + VideoEntry.COLUMN_SERVER_ID + " TEXT NOT NULL, "
            + VideoEntry.COLUMN_TITLE + " TEXT NOT NULL, " 
            + VideoEntry.COLUMN_DURATION + " TEXT, "  
            + VideoEntry.COLUMN_CONTENT_TYPE + " TEXT, " 
            + VideoEntry.COLUMN_RATING + " TEXT, " 
            + VideoEntry.COLUMN_TOTAL_RATINGS + " TEXT, "
            + VideoEntry.COLUMN_URI + " TEXT"
            + " );";
        
        // Create the table.
        db.execSQL(SQL_CREATE_VIDEO_TABLE);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
        db.execSQL("DROP TABLE IF EXISTS " 
                + VideoEntry.TABLE_NAME);
     onCreate(db);

	}

}
