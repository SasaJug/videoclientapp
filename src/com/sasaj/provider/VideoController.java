package com.sasaj.provider;

import java.io.File;
import java.util.ArrayList;


import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.sasaj.model.AverageVideoRating;
import com.sasaj.model.Video;
import com.sasaj.oauth.EasyHttpClient;
import com.sasaj.oauth.SecuredRestBuilder;
import com.sasaj.oauth.UnsafeHttpsClient;
import com.sasaj.provider.VideoContract.VideoEntry;
import com.sasaj.provider.VideoStatus.VideoState;
import com.sasaj.utils.Constants;
import com.sasaj.utils.VideoMediaStoreUtils;
import com.sasaj.utils.VideoStorageUtils;
import com.sasaj.view.LoginActivity;
import com.sasaj.webdata.VideoServiceProxy;

public class VideoController {  
	
	private static VideoController mVideoController;
	
    
	private static final String USERNAME = "admin";
	private  static final String PASSWORD = "pass";
	private static final String CLIENT_ID = "mobile";
	
	
	private final String TAG = getClass().getSimpleName();
	private Context mContext;
	private VideoServiceProxy mVideoServiceProxy;
	private ArrayList<Video> mVideoList;

	/**
     * Constructor that initializes the VideoController.
     * 
     * @param context		
     */
    private VideoController(Context context, String userName, String password) { 
        // Store the Application Context.
        mContext = context;
       
        
        // Initialize the VideoServiceProxy.
      /*  mVideoServiceProxy = new RestAdapter
            .Builder()
            .setEndpoint(Constants.SERVER_URL)
            .setLogLevel(LogLevel.BASIC)
            .build()
            .create(VideoServiceProxy.class);*/
        
		mVideoServiceProxy = new SecuredRestBuilder()
		.setLoginEndpoint(Constants.SERVER_URL + VideoServiceProxy.TOKEN_PATH)
		.setUsername(userName)
		.setPassword(password)
		.setClientId(CLIENT_ID)
		.setClient(new ApacheClient(new EasyHttpClient()))
		.setEndpoint(Constants.SERVER_URL)
		.setLogLevel(LogLevel.FULL).build()
		.create(VideoServiceProxy.class);
    }
    
    public static VideoController init (Context context, String userName, String password){
    	
    	return mVideoController = new VideoController (context,userName, password);
    }
    
    public static VideoController getOrShowLogin(Context ctx){
    	
		if (mVideoController != null) {
			return mVideoController;
		} else {
			Intent i = new Intent(ctx, LoginActivity.class);
			ctx.startActivity(i);
			return null;
		}
    	
    }

    
    /**
     *   Gets ArrayList of Video objects, stores it into the VideoProvider db,
     *  and then queries the db and sends cursor back to display.
     */
    
	public Cursor getVideoList() {

		 mVideoList = (ArrayList<Video>) mVideoServiceProxy.getVideoList();
		 Log.i(TAG,"received list: " + mVideoList.toString());
		 
		 mContext.getContentResolver().delete(VideoEntry.CONTENT_URI,null,null);
		 
			
			 for(Video video : mVideoList){
				 
				 	AverageVideoRating averageRating = mVideoServiceProxy.getVideoRating(video.getId());
					insertRow (video, averageRating.getRating(), averageRating.getTotalRatings());

			 }
			 
		 return mContext.getContentResolver().query(VideoEntry.CONTENT_URI, null, null, null, null);
		 
	}

    
    /**
     *  Inserts single row in the db which corresponds to single Video metadata object
     *  received from the service.
     */

	private Uri insertRow(Video video, double rating, int totalRatings) {
		
		ContentValues cv = new ContentValues();
		 cv.put(VideoEntry.COLUMN_SERVER_ID,video.getId());
         cv.put(VideoEntry.COLUMN_TITLE,video.getTitle());
         cv.put(VideoEntry.COLUMN_DURATION,video.getDuration());
         cv.put(VideoEntry.COLUMN_CONTENT_TYPE, video.getContentType());
         cv.put(VideoEntry.COLUMN_URI,video.getUrl());
         cv.put(VideoEntry.COLUMN_RATING, rating);
         cv.put(VideoEntry.COLUMN_TOTAL_RATINGS, totalRatings);
		
		 return mContext.getContentResolver().insert(VideoEntry.CONTENT_URI, cv);
	}

    
    /**
     *  First sends Video metadata object to the service. When the service returns Video object with added
     *  id uploads video to the service. If video is successfully uploaded returns true.
     *  
     *  @param Uri videoUri
     *  @return boolean
     */
	public boolean uploadVideo(Uri videoUri) {
		  // Get the Video from Android Video Content Provider having
        // the given Uri.
        String filePath = VideoMediaStoreUtils.getPath(mContext, videoUri);
        Video androidVideo = VideoMediaStoreUtils.getVideo(mContext, filePath); 
        
        // Check if we get any Video from Android Video Content
        // Provider.
        if (androidVideo != null) {
        	Log.i(TAG, androidVideo.toString());
            // Add the metadata of the Video to Server and get the
            // result Video that contains additional metadata
            // generated by Server.
        	
        	
            Video receivedVideo = 
                mVideoServiceProxy.addVideo(androidVideo);
            	
            // Check if the Server returns any Video metadata.
            if (receivedVideo != null) {
                // Prepare to Upload the Video data.
            	Log.i(TAG,"received video: " + receivedVideo.toString());
                // Create an instance of the file to be uploaded.
                File videoFile = new File(filePath);
                
                // Check the file size in MegaBytes.
                long size = 
                     videoFile.length() / Constants.MEGA_BYTE;

                // Check if the file size is less than the size of the
                // video that can be uploaded to the server.
                if (size < Constants.MAX_SIZE) {
                    // Finally, upload the Video data to the server
                    // and get the status of the uploaded video data.
                    VideoStatus status =
                        mVideoServiceProxy.setVideoData
                             (receivedVideo.getId(),
                              new TypedFile(receivedVideo.getContentType(),
                                            videoFile));
                    // Check if the Status of the Video or not.
                    if (status.getState() == VideoState.READY) {
                        // Video successfully uploaded.
                        return true;
                    }
                } else 
                        return false;
            }
        }
        // Error occured while uploading the video.
        return false;
	}

	
    
    /**
     *  First gets title of the selected video, then downloads it from the service, stores it in the external storage 
     *  and notifies media scanner that new video is available.
     *  
     *  @param long videoId
     *  @return boolean
     */
	public boolean downloadVideo(long videoId) {
		
		Log.i(TAG,"downloadVideo()" + Long.toString(videoId));
		String[] selectionArgs = { Long.toString(videoId) };
		
		String title;
		
		File file = null;
		// get Video from VideoProvider with same server_id
		Cursor cursor = mContext.getContentResolver().query(VideoEntry.CONTENT_URI, null, VideoEntry.SELECTION_VIDEO, selectionArgs, null);
		if (cursor.moveToFirst()){
			title=cursor.getString(cursor.getColumnIndex(VideoEntry.COLUMN_TITLE));
		} else {
			return false;
		}
		
		
		Response response = mVideoServiceProxy.getData(videoId);
		
		file = VideoStorageUtils.storeVideoInExternalDirectory(mContext, response, title);
		
		if (file != null){
			return true;
		}
		
		return false;
		
	}

    
    /**
     *  Sends videoId of the rated video and rating from the user. Service returns Video metadata object of the rated video 
     *  with average rating from all users. If returned video is not null returns true.
     *  
     *  @param long videoId
     *  @param int rating
     *  
     *  @return boolean
     */
	
	public boolean rateVideo(Long videoId, int rating) {
		Log.i(TAG, "Video " + Long.toString(videoId) + " rated with " + Integer.toString(rating) + " stars");
		AverageVideoRating averageRating = mVideoServiceProxy.rateVideo(videoId, rating);
		if (averageRating != null){
			return true;
		}
		return false;
	}

}
