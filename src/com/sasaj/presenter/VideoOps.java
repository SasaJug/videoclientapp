package com.sasaj.presenter;

import java.io.File;
import java.lang.ref.WeakReference;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.sasaj.R;
import com.sasaj.common.ConfigurableOps;
import com.sasaj.common.GenericAsyncTask;
import com.sasaj.common.GenericAsyncTaskOps;
import com.sasaj.provider.VideoController;
import com.sasaj.provider.VideoContract.VideoEntry;
import com.sasaj.services.DownloadVideoService;
import com.sasaj.services.UploadVideoService;
import com.sasaj.utils.VideoMediaStoreUtils;
import com.sasaj.view.VideoListActivity;
import com.sasaj.view.ui.VideoAdapter;


public class VideoOps implements ConfigurableOps,
								 GenericAsyncTaskOps<Void, Void, Cursor>{

    /**
     * Debugging tag used by the Android logger.
     */
    private static final String TAG =
        VideoOps.class.getSimpleName();
    
    /**
     * The GenericAsyncTask used to expand an Video in a background
     * thread via the Video web service.
     */
    private GenericAsyncTask<Void, Void, Cursor, VideoOps> mAsyncTask;
    
    /**
     * VideoController mediates the communication between Server and
     * Android Storage.
     */
	private VideoController mVideoController;

    /**
     * Used to enable garbage collection.
     */
    private WeakReference<VideoListActivity> mActivity;
    private ListView mVideosList;
    /**
     *  It allows access to application-specific resources.
     */
    private Context mApplicationContext;

	private int position;

	private VideoAdapter mAdapter;

	private Cursor cursor;

	private String mUserName;

	private String mPassword;

	
	// SimpleCursorAdapter parameters


    
    
	/**
     * Default constructor that's needed by the GenericActivity
     * framework.
     */
    public VideoOps() {
    }
    
    
	@Override
	public void onConfiguration(Activity activity, boolean firstTimeIn) {
		
		 final String time =
		            firstTimeIn 
		            ? "first time" 
		            : "second+ time";
		        
		        Log.d(TAG,
		              "onConfiguration() called the "
		              + time
		              + " with activity = "
		              + activity);
		
		    // (Re)set the mActivity WeakReference.
		    mActivity = new WeakReference<>((VideoListActivity) activity);
	        
		       mVideosList = (ListView)  mActivity.get().findViewById(R.id.videoList);

		       mVideosList.setOnItemClickListener(new OnItemClickListener() {
		       	
		       	@Override
		       	   public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						
						//sends position to videoOps
						setPosition(position);
					
						}
		       	});
	        
	    if (firstTimeIn) {
	        // Get the Application Context.
	        mApplicationContext = activity.getApplicationContext();
	        
	        // Create VideoController that will mediate the 
	        // communication between Server and Android Storage.
	        Intent intent = mActivity.get().getIntent();
	        mUserName = intent.getStringExtra("userName");
	        mPassword = intent.getStringExtra("password");
	        mVideoController = VideoController.init(mApplicationContext,mUserName, mPassword);
	        
	        // Create a local instance of our custom Adapter for our
	        // ListView.
	        mAdapter = new VideoAdapter(mApplicationContext);
	    }

	        
	    mVideosList.setAdapter(mAdapter);

	        if (cursor != null)
	        	displayVideoList();
	        else 	
	        	getVideoList();
	}
	


	public void setPosition(int position) {
		this.position = position;
		
		if(cursor.moveToPosition(position)){
			
			String downloaded =  VideoMediaStoreUtils.videoPathOnDevice(mApplicationContext, 
					                                                    cursor.getString(cursor.getColumnIndex(VideoEntry.COLUMN_TITLE))); 
		
			if (downloaded == null)
				mActivity.get().notDownloadedDialog();
			else
				mActivity.get().downloadedDialog();
			
		}
	}
    
    /**
     * Start a service that Uploads the Video having given Id.
     *   
     * @param videoId
     */
    public void uploadVideo(Uri videoUri){
    	Log.i(TAG,"Starting UploadVideoService");
        mApplicationContext.startService
            (UploadVideoService.makeIntent 
                 (mApplicationContext,
                  videoUri));
    }
    
    /**
     * Display the Videos in ListView
     * 
     * @param videos
     */
    public void displayVideoList() {
      
        if (cursor.moveToFirst()) {
            // Update the adapter with the List of Videos.
            mAdapter.changeCursor(cursor);
            Log.i(TAG, "Video meta-data loaded from Video Service");
        } else {
            Log.i(TAG, "Please connect to the Video Service");
            
            //finish();
        }
      
    }
    
    /**
     * Start a service that Downloads the Video having given Id.
     *   
     * @param videoId
     */
    public void downloadVideo(){
    	Log.i(TAG,"Starting DownloadVideoService");
    	cursor.moveToPosition(position);
    	Long videoId = cursor.getLong(cursor.getColumnIndex(VideoEntry.COLUMN_SERVER_ID));
    	Log.i(TAG, "videoId: " + Long.toString(videoId));
        mApplicationContext.startService
            (DownloadVideoService.makeIntent 
                 (mApplicationContext,
                  videoId));
    }
    

    
  
    
    /**
     * Sends rating of the video to the server.
     * 
     * @param rating
     */
	public void rateVideo(int rating) {
        RateVideo rateVideoTask = new RateVideo();
        rateVideoTask.execute(rating);
		
	}




	public void playVideo() {
		if(cursor.moveToPosition(position));
		String videoPath =   VideoMediaStoreUtils.videoPathOnDevice(mApplicationContext, 
                													cursor.getString(cursor.getColumnIndex(VideoEntry.COLUMN_TITLE)));
		//cursor.getString(cursor.getColumnIndex(VideoEntry.COLUMN_FILE_LOCATION));
		if (videoPath != null){
			Log.i(TAG, videoPath);
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoPath));
			intent.setDataAndType(Uri.fromFile(new File(videoPath)), "video/mp4");
			mActivity.get().startActivity(intent);		
		}

		
	}


    /**
     * Gets the VideoList from Server by executing the AsyncTask to
     * expand the acronym without blocking the caller.
     */
    public void getVideoList(){
        mAsyncTask = new GenericAsyncTask<>(this);
        mAsyncTask.execute();
    }
    
	@Override
	public Cursor doInBackground(Void... params) {
		
		return mVideoController.getVideoList();
	}


	@Override
	public void onPostExecute(Cursor cursor) {
		this.cursor = cursor;
		displayVideoList();
	}
	
	

	
	// Sends video rating to server
	private class RateVideo extends AsyncTask<Integer, Void, Void>{
		
		Long videoId;
		@Override
		protected void onPreExecute() {
	    	cursor.moveToPosition(position);
	    	videoId = cursor.getLong(cursor.getColumnIndex(VideoEntry.COLUMN_SERVER_ID));
		}

		@Override
		protected Void doInBackground(Integer... params) {
			
			mVideoController.rateVideo(videoId, params[0]);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			getVideoList();
		}
	}

}
