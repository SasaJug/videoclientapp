### VideoClient App ###

* This is a simple video service. There are options for recording, watching and rating of videos, as well as options for uploading videos to and downloading videos from simple server side service (gradle project of this service is in VideoServerApp repo).
* The app is not published on GooglePlay. It is developed as a final project for [Programming Cloud Services for Android Handheld Systems: Security MOOC on Coursera](https://www.coursera.org/course/mobilecloudsecurity).

* [YouTube screencast](https://youtu.be/CWxMKqzSUSc)

* Left to be implemented:

1. Check for network and service availability.
2. Loading list offline from the database.
3. Design
4. Separate activity for showing videos.
5. OAuth2.0 security and persistence of the video list (on the server side).

### Features ###

* Configuration changes are handled by GenericActivity/RetainFragmentManager/ConfigurableOps frameworks.
* Video metadata are saved in database. ContentProvider is also implemented.
* Videos are uploaded and downloaded by using IntentService. AsyncTask is used for querying database and rating.